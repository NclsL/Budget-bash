#include <stdio.h>
#include <stdlib.h>
#include <string.h> //strtok();
#include <unistd.h> //getcwd(), getuid();
#include <errno.h> //error handling
#include <pwd.h> //getpwuid(); for current user
#include <sys/types.h> //getpwuid();
#include <sys/wait.h> //wait();
#include <ctype.h> //isspace();
#include <fcntl.h>
#include <sys/stat.h>

#define WHITESPACE "\t\n\v\r "
//Horizontal tab, newline, vertical tab, return and whitespace can be used when splitting the line to tokens getArgs

typedef struct parsedCmds {
    int background;
    char* output;
    char* input;
    char** cmds;
} ParsedCmds;

void shell();
void prompt();
char* getLine();
char** getArguments(char *);
void handleCommands(ParsedCmds *);
void execCommands(ParsedCmds *);
int getParsedArgs(char *, ParsedCmds *);
char* trim(char *);

int main(int argc, char* argv[]) {
    shell();
    return 0;
}

void shell() {
    while (1) {
        char* line;
        ParsedCmds *cmdsPtr = malloc(sizeof *cmdsPtr);
        cmdsPtr->cmds   = NULL;
        cmdsPtr->output = NULL;
        cmdsPtr->input  = NULL;
        cmdsPtr->background = 0;

        prompt();
        line = getLine(); //Get user input
        int cont = getParsedArgs(line, cmdsPtr);
        if (cont)
            handleCommands(cmdsPtr);

        for (int i = 0; cmdsPtr->cmds[i] != NULL; ++i)
            free(cmdsPtr->cmds[i]);
        free(cmdsPtr->cmds);
        free(cmdsPtr->input);
        free(cmdsPtr->output);
        free(cmdsPtr);
        free(line);
        cmdsPtr->background = 0;
    }
}

void prompt() {
    char dir[1024]; //current dir
    struct passwd *pw = getpwuid(getuid()); //Current user
    char hostname[1024]; //hostname
    gethostname(hostname, 1024);

    if (getcwd(dir, 1024) != NULL && pw != NULL) //Check cwd and username are set
        printf("%s@%s %s/$ ", pw->pw_name, hostname, dir); //Print prompt "user@host cwd"
    else
        printf("budget bash prompterror");
}

/* Get line from stdin (user at terminal) with getline();, handles input of max 4096 bytes */
char* getLine() {
    size_t lineSize;
    char* line = NULL;
    if (getline(&line, &lineSize, stdin) == -1)
        printf("Rivin lukemisessa tapahtui virhe\n");
    return line;
}

//Parses arguments into the struct
int getParsedArgs(char* line, ParsedCmds* cmdsPtr) {
    int tmpStart, stop;
    char cmd[256];
    char* trimmedCmd;
    int start = strlen(line) - 1;
    int count = 0;

    // Parse each argment into the struct, categorize them as commands, input or output
    for (int i = strlen(line) - 1; i >= 0; --i) {
        if (line[i] == '&') {
            line[i] = ' ';
            cmdsPtr->background = 1;
        }
        if (i == 0) { //Copy first argument into cmd
            memcpy(cmd, &line[0], start); //Copy arg to cmd array
            cmd[start] = '\0'; //Set terminating NULL

            //Trim leading and trailing whitespace from argument
            trimmedCmd = trim(cmd);

            //Allocate memory for the 2 dimensional struct holding the commands
            if (((cmdsPtr->cmds = realloc(cmdsPtr->cmds, sizeof(*cmdsPtr->cmds) * ++count)) == NULL) ||
                ((cmdsPtr->cmds[count - 1] = malloc(sizeof(char) * strlen(trimmedCmd) + 1)) == NULL)) {
                perror("Muistin varaus epäonnistui\n");
                exit(1);
            }

            //Insert trimmed command into the struct
            strcpy(cmdsPtr->cmds[count - 1], trimmedCmd);
            cmdsPtr->cmds = realloc(cmdsPtr->cmds, sizeof(char*) * (count + 1)); //Allocate memory for one last NULL element
            cmdsPtr->cmds[count] = NULL;
            if (trimmedCmd[0] == '|' || trimmedCmd[0] == '<' || trimmedCmd[0] == '>' || strlen(trimmedCmd) == 0) { //Moved down here because at top it would 'cause seg fault (free()ing unallocated mem)
                printf("Check syntax\n");
                return 0;
            }
        } else if (line[i] == '|') { //This gets arguments 2...n, works just like the one before, just different indexes
            stop = i + 1;
            tmpStart = start;
            start = stop - 1;
            memcpy(cmd, &line[stop], tmpStart - stop + 1);
            cmd[tmpStart - stop] = '\0';
            trimmedCmd = trim(cmd);

            if (((cmdsPtr->cmds = realloc(cmdsPtr->cmds, sizeof(*cmdsPtr->cmds) * ++count)) == NULL) ||
                ((cmdsPtr->cmds[count - 1] = malloc(sizeof(char) * strlen(trimmedCmd) + 1)) == NULL)) {
                    perror("Muistin varaus epäonnistui\n");
                    exit(1);
            }

            strcpy(cmdsPtr->cmds[count - 1], trimmedCmd);
            cmdsPtr->cmds = realloc(cmdsPtr->cmds, sizeof(char*) * (count + 1));
            cmdsPtr->cmds[count] = NULL;
            if (strlen(trimmedCmd) == 0) {
                printf("Check syntax\n");
                return 0;
            }
        } else if (line[i] == '>') { //Gets output, works like the one before
            stop = i + 1;
            tmpStart = start;
            start = stop - 1;
            memcpy(cmd, &line[stop], tmpStart - stop + 1);
            cmd[tmpStart - stop] = '\0';
            trimmedCmd = trim(cmd);
            cmdsPtr->output = (char*)malloc(strlen(trimmedCmd) * sizeof(char) + 1);
            strcpy(cmdsPtr->output, trimmedCmd);
        } else if (line[i] == '<') { //Gets output
            stop = i + 1;
            tmpStart = start;
            start = stop - 1;
            memcpy(cmd, &line[stop], tmpStart - stop + 1);
            cmd[tmpStart - stop] = '\0';
            trimmedCmd = trim(cmd);
            cmdsPtr->input = (char*)malloc(strlen(trimmedCmd) * sizeof(char) + 1);
            strcpy(cmdsPtr->input, trimmedCmd);
        }
    }

    //Reorganize arguments in the struct cmds**
    int k = 0;
    char* temp;
    while (k < count) {
        temp = cmdsPtr->cmds[k];
        cmdsPtr->cmds[k] = cmdsPtr->cmds[count - 1];
        cmdsPtr->cmds[count - 1] = temp;
        ++k; --count;
    }
    return 1;
}

//Trims trailing and leading whitespace
char* trim(char *cmd) {
    while (isspace(cmd[0])) ++cmd;
    char* cmdEnd = cmd + strlen(cmd) - 1;
    while (cmd < cmdEnd && isspace(*cmdEnd)) --cmdEnd;
    *(cmdEnd + 1) = 0;
    return cmd;
}

//Returns a pointer to the list of arguments is this needed?
char** getArguments(char* line) {
    int index = 0;
    char*  arg  = strtok(line, WHITESPACE); //First argument
    char** args = NULL; //List of arguments

    //Split when encounters whitespace with the help of strtok();, dynamically reallocs memory for new arguments
    while (arg) {
        if ((args = realloc(args, sizeof(char*) * ++index)) == NULL) {
            perror("Muistin varaus epäonnistui\n");
            exit(1);
        }

        args[index - 1] = arg; //Insert each word into args and move onto next word
        arg = strtok(NULL, WHITESPACE);
    }

    //Realloc for the last NULL
    args = realloc(args, sizeof(char*) * (index + 1));
    args[index] = NULL;
    return args;
}

void handleCommands(ParsedCmds *cmdsPtr){
    char *temp = (char*)malloc((strlen(cmdsPtr->cmds[0])+1) * sizeof(char));
    memcpy(temp, cmdsPtr->cmds[0], strlen(cmdsPtr->cmds[0]));
    temp[strlen(cmdsPtr->cmds[0])] = '\0';
    char **command1 = getArguments(temp);
    if(strcmp(command1[0], "cd") == 0){
        if((command1[1] == NULL) || (strcmp(command1[1], "~") == 0)){
            chdir(getenv("HOME"));
        }else if(chdir(command1[1]) != 0){
            perror("error in cd");
        }
    }else if(strcmp(command1[0], "exit") == 0) {
        exit(0);
    }else {
        execCommands(cmdsPtr);
    }
    free(temp);
    free(command1);
}

void execCommands(ParsedCmds *cmdsPtr){
    pid_t pid;
    int input = 0, output = 0, pipeNum = 0;

    if(cmdsPtr->input != NULL){
        if((input = open(cmdsPtr->input, O_RDONLY)) < 0){
            perror("inputfile");
            exit(1);
        }
    }

    if(cmdsPtr->output != NULL){
        if((output = open(cmdsPtr->output, O_WRONLY | O_CREAT, 0666)) < 0){
            perror("outputfile");
            exit(1);
        }
    }

    for(int i = 1; cmdsPtr->cmds[i] != NULL; ++i){
        ++pipeNum;
    }

    int pipefds[2*pipeNum];

    pid_t children[pipeNum+1];

    for(int i = 0; i < pipeNum; i++){
        if(pipe(pipefds + i*2) < 0) {
            perror("pipe");
            exit(1);
        }
    }

    for(int i = 0; i < pipeNum+1; i++){
        pid = fork();

        char *temp = (char*)malloc((strlen(cmdsPtr->cmds[i]) + 1) * sizeof(char));
        memcpy(temp, cmdsPtr->cmds[i], strlen(cmdsPtr->cmds[i]));
        temp[strlen(cmdsPtr->cmds[i])] = '\0';
        char **args = getArguments(temp);

        if(pid == 0){
            if((i == 0) && (input != 0)){
                dup2(input, STDIN_FILENO);
                close(input);

            }
            if((i == pipeNum) && (output != 0)){
                dup2(output, STDOUT_FILENO);
                close(output);

            }
            if(i != 0){
                if(dup2(pipefds[(i-1)*2], 0) < 0){
                    perror("dup2");
                    exit(1);
                }
            }
            if(i != pipeNum){
                if(dup2(pipefds[i*2+1], 1) < 0) {
                    perror("dup2");
                    exit(1);
                }
            }
            for(int i = 0; i < 2*pipeNum; ++i){
                close(pipefds[i]);
            }
            if(execvp(args[0], args) == -1){
                printf("command %s was not found\n", args[0]);
            }
        }else if (pid < 0){
            perror("error");
            exit(1);
        }else {
            children[i] = pid;
        }
        free(temp);
        free(args);
    }

    for(int i = 0; i < 2*pipeNum; i++){
        close(pipefds[i]);
    }

    if(cmdsPtr->background != 1){
        for(int i = 0; i < pipeNum+1; i++){
            waitpid(children[i], NULL, 0);
        }
    }
}
